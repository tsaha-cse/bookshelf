package com.bookshelf.data.datasource.remote.api

import co.infinum.retromock.meta.Mock
import co.infinum.retromock.meta.MockBehavior
import co.infinum.retromock.meta.MockResponse
import co.infinum.retromock.meta.MockResponses
import com.bookshelf.data.model.BookDetailsApiModel
import com.bookshelf.data.model.BookResponseApiModel
import retrofit2.Response
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface BookMockApiService : BookService {

    @Mock
    @MockBehavior(durationDeviation = 500, durationMillis = 1000)
    @MockResponses(
        MockResponse(body = "mock/book_response_index_1.json"),
        MockResponse(body = "mock/book_response_index_2.json"),
        MockResponse(body = "mock/book_response_index_3.json")
    )
    @GET("/api/v1/items")
    override suspend fun getBooks(
        @Query("offset") offset: Int,
        @Query("count") count: Int
    ): BookResponseApiModel

    @Mock
    @MockResponse(body = "mock/book_response.json")
    @GET("/api/v1/items/{id}")
    override suspend fun getBookDetails(@Query("id") bookId: String): BookDetailsApiModel?

    @Mock
    @MockResponse(body = "mock/books_response.json")
    @GET("/api/v1/all_items")
    suspend fun getAllBooks(): List<BookDetailsApiModel>

    @Mock
    @FormUrlEncoded
    @MockResponse(code = 201, body = "mock/book_add_success_response.json")
    @MockBehavior(durationMillis = 2000)
    @POST("/api/v1/addbook")
    override suspend fun addBook(
        title: String,
        author: String,
        price: Double,
        imageFileName: String
    ): Response<Unit>
}
