package com.bookshelf.booklist

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bookshelf.booklist.BookListViewModel.BookUiModel
import com.bookshelf.booklist.BookListViewModel.BookUiModel.*
import com.bookshelf.extension.onClickDebounced

internal class BookListAdapter(
    private val listener: (BookState) -> Unit
) : ListAdapter<BookUiModel, BookListAdapter.ItemViewHolder>(BookDiffCallback()) {

    private companion object {
        private const val VIEW_TYPE_CONTENT = 0
        private const val VIEW_TYPE_LOADER = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder =
        when (viewType) {
            VIEW_TYPE_CONTENT -> {
                ItemViewHolder(BookInfoView(parent.context)).apply {
                    itemView.onClickDebounced {
                        if (bindingAdapterPosition != RecyclerView.NO_POSITION) {
                            listener(getItem(bindingAdapterPosition) as BookState)
                        }
                    }
                }
            }
            VIEW_TYPE_LOADER -> ItemViewHolder(LoadingView(parent.context))
            else -> throw IllegalArgumentException("Invalid type")
        }


    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        when (holder.itemViewType) {
            VIEW_TYPE_CONTENT -> {
                holder.itemView as BookInfoView
                val book = (getItem(position) as BookState).book
                holder.itemView.renderContent(book.title, book.author)
            }
            VIEW_TYPE_LOADER -> {
                holder.itemView as LoadingView
            }
        }
    }

    override fun getItemViewType(position: Int): Int =
        when (getItem(position)) {
            is BookState -> VIEW_TYPE_CONTENT
            is Loader -> VIEW_TYPE_LOADER
        }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class BookDiffCallback : DiffUtil.ItemCallback<BookUiModel>() {
        override fun areItemsTheSame(oldItem: BookUiModel, newItem: BookUiModel): Boolean =
            if (oldItem is BookState && newItem is BookState) {
                oldItem.book.id == newItem.book.id
            } else {
                oldItem == newItem
            }

        override fun areContentsTheSame(oldItem: BookUiModel, newItem: BookUiModel): Boolean =
            oldItem == newItem
    }
}
