package com.bookshelf

import android.app.Application
import com.bookshelf.addbook.di.addBookModule
import com.bookshelf.booklist.di.bookListModule
import com.bookshelf.data.di.dataComponent
import com.bookshelf.di.homeModule
import com.bookshelf.platform.APPLICATION_BG
import com.bookshelf.platform.APPLICATION_MAIN
import com.bookshelf.bookdetail.di.bookDetailModule
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BookShelfApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
        setupThreadingContexts()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@BookShelfApp)
            modules(dataComponent)
            modules(homeModule)
            modules(bookListModule)
            modules(bookDetailModule)
            modules(addBookModule)
        }
    }

    private fun setupThreadingContexts() {
        APPLICATION_MAIN = Dispatchers.Main + CoroutineExceptionHandler { _, error -> throw error }
        APPLICATION_BG = Dispatchers.Default + CoroutineExceptionHandler { _, error -> throw error }
    }
}
