package com.bookshelf.addbook.di

import com.bookshelf.addbook.AddBookViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val addBookModule = module {
    viewModel { AddBookViewModel(get()) }
}
