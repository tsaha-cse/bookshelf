package com.bookshelf.data.datasource.remote.api

import com.bookshelf.data.model.BookDetailsApiModel
import com.bookshelf.data.model.BookResponseApiModel

class BookApiImpl(private val bookService: BookApiService) : BookApi {

    override suspend fun getBooks(offset: Int, count: Int): BookResponseApiModel =
        bookService.getBooks(offset, count)

    override suspend fun getBookDetail(bookId: String): BookDetailsApiModel? =
        bookService.getBookDetails(bookId)

    override suspend fun addBook(
        title: String,
        author: String,
        price: Double,
        imageFileName: String
    ) {
        bookService.addBook(title, author, price, imageFileName)
    }
}
