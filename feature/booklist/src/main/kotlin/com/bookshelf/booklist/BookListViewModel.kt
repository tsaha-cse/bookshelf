package com.bookshelf.booklist

import com.bookshelf.booklist.BookListViewModel.BookUiModel.*
import com.bookshelf.data.model.BookModel
import com.bookshelf.data.repository.BookRepository
import com.bookshelf.platform.BaseViewModel
import kotlinx.coroutines.flow.*
import timber.log.Timber

class BookListViewModel(
    private val bookRepository: BookRepository
) : BaseViewModel() {

    sealed class BookUiModel {
        data class BookState(val book: BookModel) : BookUiModel()
        object Loader : BookUiModel()
    }

    data class ViewState(val bookUiModels: List<BookUiModel>)

    data class NavState(val bookId: String)

    val isLastPage: Boolean = !bookRepository.hasNext
    val isLoading: Boolean = bookRepository.isLoading()
    private val _viewState: MutableStateFlow<ViewState> = MutableStateFlow(ViewState(emptyList()))
    val viewState: StateFlow<ViewState> = _viewState.asStateFlow()

    private val _navState: MutableSharedFlow<NavState> = MutableSharedFlow()
    val navState: SharedFlow<NavState> = _navState.asSharedFlow()

    fun onCreate() {
        observeBooks()
    }

    private fun observeBooks() {
        combine(
            bookRepository.observeBooks(),
            bookRepository.observeLoading()
        ) { books, isLoading -> _viewState.emit(ViewState(books.toBookUiModels(isLoading))) }
            .catch { Timber.e(it) }
            .launchIn(this)
    }

    fun onBookTapped(bookState: BookState) {
        coroutineWrapper { _navState.emit(NavState(bookState.book.id)) }
    }

    fun loadMoreBooks() {
        coroutineWrapper { bookRepository.loadMoreBooks() }
    }

    private fun List<BookModel>.toBookUiModels(isLoading: Boolean): List<BookUiModel> {
        val uiModels = mutableListOf<BookUiModel>()
        uiModels.addAll(map { BookState(it) })
        if (isLoading) {
            uiModels.add(Loader)
        }
        return uiModels
    }
}
