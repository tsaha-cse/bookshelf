package com.bookshelf.data.datasource.remote.di

import com.bookshelf.data.datasource.remote.api.BookApiService
import org.koin.dsl.module
import retrofit2.Retrofit

val apiModule = module {
    single<BookApiService> {
        val builder: Retrofit.Builder = get()
        builder
            .baseUrl(BOOK_API)
            .build()
            .create(BookApiService::class.java)
    }
}
