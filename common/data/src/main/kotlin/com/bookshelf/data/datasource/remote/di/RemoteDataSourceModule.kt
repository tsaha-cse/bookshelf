package com.bookshelf.data.datasource.remote.di

import com.bookshelf.data.datasource.remote.BookApiDataSource
import org.koin.dsl.module

val remoteDataSourceModule = module {
    single { BookApiDataSource(get()) }
}
