package com.bookshelf.data

import com.bookshelf.data.model.BookApiModel
import com.bookshelf.data.model.BookDetailsApiModel
import com.bookshelf.data.model.BookDetailsModel
import com.bookshelf.data.model.BookModel

internal fun List<BookApiModel>.toBookList(): List<BookModel> =
    map {
        BookModel(
            id = it.id,
            link = it.link,
            title = it.title,
            author = it.author
        )
    }

internal fun BookDetailsApiModel.toBookDetailModel(): BookDetailsModel =
    BookDetailsModel(
        id = this.id,
        link = this.link,
        title = this.title,
        author = this.author,
        country = this.country,
        imageLink = this.imageLink,
        language = this.language,
        pageTotal = this.pageTotal,
        year = this.year
    )
