package com.bookshelf.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bookshelf.INTENT_ACTION_ADD_BOOK
import com.bookshelf.R
import com.bookshelf.databinding.ActivityHomeBinding
import com.bookshelf.platform.extension.internalIntent

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ActivityHomeBinding.inflate(layoutInflater).apply {
            setContentView(root)
            setupMenu(this)
        }
    }

    private fun setupMenu(binding: ActivityHomeBinding) {
        binding.toolbar.menu.findItem(R.id.menu_item_add).setOnMenuItemClickListener {
            startActivity(internalIntent(this, INTENT_ACTION_ADD_BOOK))
            true
        }
    }
}
