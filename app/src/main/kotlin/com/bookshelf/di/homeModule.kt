package com.bookshelf.di

import com.bookshelf.homecontroller.HomeControllerViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val homeModule = module {
    viewModel {
        HomeControllerViewModel(get())
    }
}
