package com.bookshelf.data.exception

object EmptyBookReponseException : RuntimeException("No book information found")