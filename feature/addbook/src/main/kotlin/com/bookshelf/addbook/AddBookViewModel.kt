package com.bookshelf.addbook

import com.bookshelf.data.repository.BookRepository
import com.bookshelf.platform.BaseViewModel
import com.bookshelf.platform.extension.onFailure
import com.bookshelf.platform.extension.onSuccess
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asSharedFlow

class AddBookViewModel(
    private val repository: BookRepository
) : BaseViewModel() {

    sealed class ViewState {
        object Loading : ViewState()
        object Success : ViewState()
        object Failure : ViewState()
    }

    private val _viewState: MutableSharedFlow<ViewState> = MutableSharedFlow()
    val viewState: SharedFlow<ViewState> = _viewState.asSharedFlow()

    fun onClickAddBook(title: String, author: String, price: Double, imageFileName: String) {
        coroutineWrapper {
            _viewState.emit(ViewState.Loading)
            repository.addBook(title, author, price, imageFileName)
                .onSuccess { coroutineWrapper { _viewState.emit(ViewState.Success) } }
                .onFailure { coroutineWrapper { _viewState.emit(ViewState.Failure) } }
        }
    }
}
