package com.bookshelf.data.di

import com.bookshelf.data.datasource.remote.di.*
import com.bookshelf.data.repository.di.repositoryModule

val dataComponent = listOf(
    apiConfigModule,
    apiModule,
    mockApiModule,
    bookApiModule,
    remoteDataSourceModule,
    repositoryModule
)
