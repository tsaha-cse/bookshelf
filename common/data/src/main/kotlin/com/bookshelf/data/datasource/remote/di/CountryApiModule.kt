package com.bookshelf.data.datasource.remote.di

import com.bookshelf.data.BuildConfig
import com.bookshelf.data.datasource.remote.api.BookApiImpl
import com.bookshelf.data.datasource.remote.api.BookApiService
import com.bookshelf.data.datasource.remote.api.BookMockApiImpl
import com.bookshelf.data.datasource.remote.api.BookMockApiService
import org.koin.dsl.module

val bookApiModule = module {
    single {
        if (BuildConfig.FLAVOR == "local") {
            BookMockApiImpl(get<BookMockApiService>())
        } else {
            BookApiImpl(get<BookApiService>())
        }
    }
}
