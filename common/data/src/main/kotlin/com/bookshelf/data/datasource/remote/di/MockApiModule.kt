package com.bookshelf.data.datasource.remote.di

import android.content.Context
import co.infinum.retromock.Retromock
import com.bookshelf.data.datasource.remote.api.BookMockApiService
import org.koin.dsl.module
import retrofit2.Retrofit

val mockApiModule = module {
    single {
        val builder: Retrofit.Builder = get()
        Retromock.Builder()
            .retrofit(builder.baseUrl(BOOK_API).build())
            .defaultBodyFactory(get<Context>().assets::open)
            .build()
    }

    single<BookMockApiService> {
        get<Retromock>().create(BookMockApiService::class.java)
    }
}
