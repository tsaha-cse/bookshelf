package com.bookshelf.data.repository.di

import com.bookshelf.data.repository.BookRepository
import org.koin.dsl.module

val repositoryModule = module {
    single { BookRepository(get()) }
}
