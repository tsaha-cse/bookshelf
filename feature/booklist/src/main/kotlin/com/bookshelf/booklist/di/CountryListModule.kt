package com.bookshelf.booklist.di

import com.bookshelf.booklist.BookListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val bookListModule = module {
    viewModel {
        BookListViewModel(get())
    }
}
