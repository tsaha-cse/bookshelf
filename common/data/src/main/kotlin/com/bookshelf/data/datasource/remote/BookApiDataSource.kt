package com.bookshelf.data.datasource.remote

import com.bookshelf.data.datasource.remote.api.BookApi
import com.bookshelf.data.model.BookDetailsApiModel
import com.bookshelf.data.model.BookResponseApiModel

class BookApiDataSource(private val bookApi: BookApi) {

    suspend fun getBooks(offset: Int = 0, count: Int = 0): BookResponseApiModel =
        bookApi.getBooks(offset, count)

    suspend fun getBookDetail(bookId: String): BookDetailsApiModel? =
        bookApi.getBookDetail(bookId)

    suspend fun addBook(
        title: String,
        author: String,
        price: Double,
        imageFileName: String
    ) = bookApi.addBook(title, author, price, imageFileName)
}
