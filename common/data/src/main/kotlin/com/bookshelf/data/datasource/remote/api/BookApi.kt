package com.bookshelf.data.datasource.remote.api

import com.bookshelf.data.model.BookDetailsApiModel
import com.bookshelf.data.model.BookResponseApiModel

interface BookApi {
    suspend fun getBooks(offset: Int = 0, count: Int = 0): BookResponseApiModel
    suspend fun getBookDetail(bookId: String): BookDetailsApiModel?
    suspend fun addBook(title: String, author: String, price: Double, imageFileName: String)
}
