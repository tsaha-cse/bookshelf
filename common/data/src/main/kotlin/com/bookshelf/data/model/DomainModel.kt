package com.bookshelf.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BookModel(
    val id: String,
    val link: String,
    val title: String,
    val author: String
) : Parcelable

@Parcelize
data class BookDetailsModel(
    val id: String,
    val link: String,
    val title: String,
    val author: String,
    val country: String,
    val imageLink: String,
    val language: String,
    val pageTotal: Int,
    val year: Int
) : Parcelable
