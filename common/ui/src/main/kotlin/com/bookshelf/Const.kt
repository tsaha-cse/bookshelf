package com.bookshelf

const val EXTRA_BOOK_ID = "extra_book_id"
const val INTENT_ACTION_BOOK_DETAIL = "com.bookshelf.bookdetail.open"
const val INTENT_ACTION_ADD_BOOK = "com.bookshelf.addbook.open"