package com.bookshelf.data.exception

object NetworkException : RuntimeException("Network connection is unavailable")