package com.bookshelf.homecontroller

import com.bookshelf.data.repository.BookRepository
import com.bookshelf.platform.BaseViewModel
import com.bookshelf.platform.extension.onFailure
import com.bookshelf.platform.extension.onSuccess
import kotlinx.coroutines.flow.*
import timber.log.Timber

class HomeControllerViewModel(
    private val bookRepository: BookRepository
) : BaseViewModel() {

    sealed class ViewState {
        object Loading : ViewState()
        object Data : ViewState()
        object GenericError : ViewState()
    }

    private val _viewState: MutableStateFlow<ViewState> = MutableStateFlow(ViewState.Loading)
    val viewState: StateFlow<ViewState> = _viewState.asStateFlow()

    fun onCreate() {
        fetchBooks()
        observeBooks()
    }

    private fun fetchBooks() {
        coroutineWrapper {
            _viewState.emit(ViewState.Loading)
            bookRepository.fetchBooks()
                .onSuccess { coroutineWrapper { _viewState.emit(ViewState.Data) } }
                .onFailure { coroutineWrapper { _viewState.emit(ViewState.GenericError) } }
        }
    }

    private fun observeBooks() {
        bookRepository.observeBooks()
            .onEach {
                coroutineWrapper {
                    if (_viewState.value == ViewState.GenericError && it.isNotEmpty()) {
                        _viewState.emit(ViewState.Data)
                    }
                }
            }
            .catch { Timber.e(it) }
            .launchIn(this)
    }
}
