package com.bookshelf.bookdetail

import com.bookshelf.data.model.BookDetailsModel
import com.bookshelf.platform.BaseViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asSharedFlow

class BookDetailViewModel(
    private val bookDetail: BookDetailsModel,
) : BaseViewModel() {

    data class ViewState(val bookDetail: BookDetailsModel)

    private val _viewState: MutableSharedFlow<ViewState> =
        MutableSharedFlow(replay = 1, onBufferOverflow = BufferOverflow.DROP_OLDEST)
    val viewState: SharedFlow<ViewState> = _viewState.asSharedFlow()

    fun onCreate() {
        coroutineWrapper { _viewState.emit(ViewState(bookDetail)) }
    }
}
