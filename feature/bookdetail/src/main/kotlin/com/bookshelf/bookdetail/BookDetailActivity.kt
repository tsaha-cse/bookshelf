package com.bookshelf.bookdetail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bookshelf.EXTRA_BOOK_ID
import com.bookshelf.bookdetail.databinding.ActivityBookDetailBinding

class BookDetailActivity : AppCompatActivity() {

    private val bookId: String by lazy {
        intent.extras?.getString(EXTRA_BOOK_ID)
            ?: throw IllegalArgumentException("Intent without $EXTRA_BOOK_ID is not allowed")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ActivityBookDetailBinding.inflate(layoutInflater).apply {
            setContentView(root)
            toolbar.setNavigationOnClickListener { finish() }
            supportFragmentManager.beginTransaction()
                .add(R.id.detail_controller, BookDetailControllerFragment.newInstance(bookId))
                .commit()
        }
    }
}
