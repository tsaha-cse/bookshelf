package com.bookshelf.addbook

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.bookshelf.addbook.AddBookViewModel.ViewState
import com.bookshelf.addbook.databinding.FragmentAddBookBinding
import com.bookshelf.addbook.extension.textChanges
import com.bookshelf.extension.onClickDebounced
import com.bookshelf.platform.extension.viewLifecycle
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddBookFragment : Fragment() {

    companion object {
        fun newInstance() = AddBookFragment()
    }

    private var binding: FragmentAddBookBinding by viewLifecycle()
    private val addBookViewModel: AddBookViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentAddBookBinding.inflate(inflater, container, false)
        .apply {
            binding = this
            initEditFields()
            initClickListener()
            initObserver()
        }
        .root

    private fun initClickListener() {
        binding.apply {
            button.onClickDebounced {
                addBookViewModel.onClickAddBook(
                    title.text.toString().trim(),
                    author.text.toString().trim(),
                    price.text.toString().trim().toDouble(),
                    image.text.toString().trim()
                )
            }
        }
    }

    private fun initEditFields() {
        binding.apply {
            combine(
                title.textChanges(),
                author.textChanges(),
                price.textChanges(),
                image.textChanges()
            ) { title, author, price, image ->
                button.isEnabled =
                    title.isNotBlank() && author.isNotBlank() && price.isNotBlank() && image.isNotBlank()
            }.launchIn(lifecycleScope)
        }
    }

    private fun initObserver() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                addBookViewModel.viewState.collectLatest { state ->
                    when (state) {
                        ViewState.Failure -> {
                            Toast.makeText(
                                requireContext(),
                                getString(R.string.add_book_message_failure),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        ViewState.Loading -> {
                            Toast.makeText(
                                requireContext(),
                                getString(R.string.add_book_message_loading),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        ViewState.Success -> {
                            Toast.makeText(
                                requireContext(),
                                getString(R.string.add_book_message_success),
                                Toast.LENGTH_SHORT
                            ).show()
                            requireActivity().finish()
                        }
                    }
                }
            }
        }
    }
}
