package com.bookshelf.addbook

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bookshelf.addbook.databinding.ActivityAddBookBinding

class AddBookActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ActivityAddBookBinding.inflate(layoutInflater).apply {
            setContentView(root)
            toolbar.setNavigationOnClickListener { finish() }
            supportFragmentManager.beginTransaction()
                .add(R.id.container, AddBookFragment.newInstance())
                .commit()
        }
    }
}
