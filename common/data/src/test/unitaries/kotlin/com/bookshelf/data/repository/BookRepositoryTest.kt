package com.bookshelf.data.repository

import app.cash.turbine.test
import arrow.core.Either
import com.bookshelf.data.datasource.remote.BookApiDataSource
import com.bookshelf.data.model.BookApiModel
import com.bookshelf.data.model.BookResponseApiModel
import com.bookshelf.data.toBookList
import com.bookshelf.unittestingtools.MainCoroutineScopeExtension
import com.google.common.truth.Truth.assertThat
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MainCoroutineScopeExtension::class)
@OptIn(ExperimentalCoroutinesApi::class)
internal class BookRepositoryTest {

    private val mockRemoteDataSource: BookApiDataSource = mockk()
    private val mockBookApiModel = BookApiModel(
        id = "anyId",
        link = "anyLink",
        title = "anyTitle",
        author = "anyAuthor"
    )
    private val mockBookResponseApiModel: BookResponseApiModel = mockk {
        every { hasNext } returns true
        every { page } returns 1
        every { books } returns listOf(mockBookApiModel)
    }
    private lateinit var repository: BookRepository

    @BeforeEach
    fun setUp() {
        repository = BookRepository(mockRemoteDataSource)
    }

    @Test
    fun `Should fetch book list`() =
        runBlockingTest {
            coEvery { mockRemoteDataSource.getBooks() } returns mockBookResponseApiModel
            assertThat(repository.fetchBooks()).isEqualTo(Either.Right(Unit))
        }

    @Test
    fun `Should observe books as expected`() =
        runBlockingTest {
            coEvery { mockRemoteDataSource.getBooks() } returns mockBookResponseApiModel
            repository.observeBooks().test {
                repository.fetchBooks()
                assertEquals(listOf(mockBookApiModel).toBookList(), awaitItem())
                cancelAndConsumeRemainingEvents()
            }
        }

    @Test
    fun `Should load more books when has next`() =
        runBlockingTest {
            coEvery { mockRemoteDataSource.getBooks() } returns mockBookResponseApiModel
            assertThat(repository.loadMoreBooks()).isEqualTo(Either.Right(true))
        }

    @Test
    fun `Should not load more books when has next`() =
        runBlockingTest {
            every { mockBookResponseApiModel.hasNext } returns false
            coEvery { mockRemoteDataSource.getBooks() } returns mockBookResponseApiModel
            repository.fetchBooks()
            assertThat(repository.loadMoreBooks()).isEqualTo(Either.Right(false))
        }
}
