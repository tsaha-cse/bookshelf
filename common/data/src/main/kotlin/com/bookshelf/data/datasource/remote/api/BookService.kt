package com.bookshelf.data.datasource.remote.api

import com.bookshelf.data.model.BookDetailsApiModel
import com.bookshelf.data.model.BookResponseApiModel
import retrofit2.Response

interface BookService {
    suspend fun getBooks(offset: Int, count: Int): BookResponseApiModel

    suspend fun getBookDetails(bookId: String): BookDetailsApiModel?

    suspend fun addBook(
        title: String,
        author: String,
        price: Double,
        imageFileName: String
    ): Response<Unit>
}
