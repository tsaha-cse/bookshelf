package com.bookshelf.data.exception

object BookDetailNotFoundException : RuntimeException("No book information found")