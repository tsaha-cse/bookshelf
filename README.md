# BookShelf: Demonstrate how to mock API responses 

# Tech Stack
    - Language: Kotlin
    - Architecture: Clean Code + MVVM + Multi module + Koin (DI)
    - Asynchrony: Coroutines + Flows
    - Network: Retrofit + Moshi
    - Network Mock: [RetroMock](https://github.com/infinum/Retromock/) 
    - Test: Junit 5 + Flow Turbine (repository is under unit test)

# Features
    - App shows result from local inbuilt json files using RetroMock.
    - RetroMock is enabled under `local` flavor. Move to `staging` or `production` flavor will enable the real retorfit to handle the ongoing request.
    - No extra changes is required to switch from mock response to live response since the implementation is agnostic in data source level.
