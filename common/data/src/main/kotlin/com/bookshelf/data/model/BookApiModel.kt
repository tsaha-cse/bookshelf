package com.bookshelf.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BookResponseApiModel(
    val page: Int,
    val hasNext: Boolean,
    @Json(name = "items")
    val books: List<BookApiModel>
)

@JsonClass(generateAdapter = true)
data class BookApiModel(
    val id: String,
    val link: String,
    val title: String,
    val author: String
)

@JsonClass(generateAdapter = true)
data class BookDetailsApiModel(
    val id: String,
    val link: String,
    val title: String,
    val author: String,
    val country: String,
    val imageLink: String,
    val language: String,
    @Json(name = "pages")
    val pageTotal: Int,
    val year: Int
)
