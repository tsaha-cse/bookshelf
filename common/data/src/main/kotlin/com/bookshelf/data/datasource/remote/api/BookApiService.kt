package com.bookshelf.data.datasource.remote.api

import com.bookshelf.data.model.BookDetailsApiModel
import com.bookshelf.data.model.BookResponseApiModel
import retrofit2.Response
import retrofit2.http.*

interface BookApiService : BookService {

    @GET("/api/v1/items")
    override suspend fun getBooks(
        @Query("offset") offset: Int,
        @Query("count") count: Int
    ): BookResponseApiModel

    @GET("/api/v1/items/{id}")
    override suspend fun getBookDetails(@Query("id") bookId: String): BookDetailsApiModel?

    @FormUrlEncoded
    @POST("/api/v1/addbook")
    override suspend fun addBook(
        @Field(value = "title") title: String,
        @Field(value = "author") author: String,
        @Field(value = "price") price: Double,
        @Field(value = "imageFileName") imageFileName: String
    ): Response<Unit>
}
