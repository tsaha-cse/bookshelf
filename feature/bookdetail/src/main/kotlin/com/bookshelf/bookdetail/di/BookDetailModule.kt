package com.bookshelf.bookdetail.di

import com.bookshelf.bookdetail.BookDetailControllerViewModel
import com.bookshelf.bookdetail.BookDetailViewModel
import com.bookshelf.data.model.BookDetailsModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val bookDetailModule = module {
    viewModel { (bookId: String) ->
        BookDetailControllerViewModel(bookId, get())
    }

    viewModel { (bookDetails: BookDetailsModel) ->
        BookDetailViewModel(bookDetails)
    }
}
