package com.bookshelf.bookdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.bookshelf.bookdetail.databinding.FragmentBookDetailBinding
import com.bookshelf.data.model.BookDetailsModel
import com.bookshelf.platform.extension.fragmentArgs
import com.bookshelf.platform.extension.viewLifecycle
import com.bookshelf.platform.extension.withArguments
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class BookDetailFragment : Fragment() {

    companion object {
        private const val ARGUMENT_BOOKING_DETAIL = "argument:book_detail"
        fun newInstance(bookDetailModel: BookDetailsModel) =
            BookDetailFragment().withArguments(ARGUMENT_BOOKING_DETAIL to bookDetailModel)
    }

    private var binding: FragmentBookDetailBinding by viewLifecycle()
    private val bookDetailViewModel: BookDetailViewModel by viewModel {
        val bookDetailModel: BookDetailsModel by fragmentArgs(ARGUMENT_BOOKING_DETAIL)
        parametersOf(bookDetailModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentBookDetailBinding.inflate(inflater, container, false)
        .apply {
            binding = this
            initObserver()
            bookDetailViewModel.onCreate()
        }
        .root

    private fun initObserver() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                bookDetailViewModel.viewState.collectLatest { book ->
                    binding.apply {
                        title.text = book.bookDetail.title
                        author.text = getString(R.string.text_author, book.bookDetail.author)
                        country.text = getString(R.string.text_country, book.bookDetail.country)
                        language.text = getString(R.string.text_language, book.bookDetail.language)
                        page.text = getString(R.string.text_page, book.bookDetail.pageTotal)
                        year.text = getString(R.string.text_year, book.bookDetail.year)
                        link.text = getString(R.string.text_link, book.bookDetail.link)
                    }
                }
            }
        }
    }
}
