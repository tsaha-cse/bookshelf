package com.bookshelf.bookdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.bookshelf.GenericErrorFragment
import com.bookshelf.LoaderFragment
import com.bookshelf.bookdetail.BookDetailControllerViewModel.ViewState
import com.bookshelf.bookdetail.databinding.FragmentBookControllerDetailBinding
import com.bookshelf.platform.extension.fragmentArgs
import com.bookshelf.platform.extension.replaceIfNoPrevious
import com.bookshelf.platform.extension.viewLifecycle
import com.bookshelf.platform.extension.withArguments
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class BookDetailControllerFragment : Fragment() {

    companion object {
        private const val ARGUMENT_BOOK_ID = "argument:book_id"
        fun newInstance(bookId: String) =
            BookDetailControllerFragment().withArguments(ARGUMENT_BOOK_ID to bookId)
    }

    private var binding: FragmentBookControllerDetailBinding by viewLifecycle()

    private val bookDetailControllerViewModel: BookDetailControllerViewModel by viewModel {
        val bookId: String by fragmentArgs(ARGUMENT_BOOK_ID)
        parametersOf(bookId)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentBookControllerDetailBinding.inflate(inflater, container, false)
        .apply {
            binding = this
            initObserver()
            bookDetailControllerViewModel.onCreate()
        }
        .root

    private fun initObserver() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                bookDetailControllerViewModel.viewState.collectLatest { viewState ->
                    when (viewState) {
                        is ViewState.Data -> {
                            replaceIfNoPrevious(binding.detailContainer.id) {
                                BookDetailFragment.newInstance(viewState.detailModel)
                            }
                        }
                        ViewState.Empty -> {
                            replaceIfNoPrevious(binding.detailContainer.id) { GenericErrorFragment.newInstance() }
                        }
                        ViewState.Loading -> {
                            replaceIfNoPrevious(binding.detailContainer.id) { LoaderFragment.newInstance() }
                        }
                    }
                }
            }
        }
    }
}
