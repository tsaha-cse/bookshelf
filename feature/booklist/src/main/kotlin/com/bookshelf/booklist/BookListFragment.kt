package com.bookshelf.booklist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.bookshelf.EXTRA_BOOK_ID
import com.bookshelf.INTENT_ACTION_BOOK_DETAIL
import com.bookshelf.booklist.databinding.FragmentBookListBinding
import com.bookshelf.platform.extension.internalIntent
import com.bookshelf.platform.extension.viewLifecycle
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class BookListFragment : Fragment() {

    private var binding: FragmentBookListBinding by viewLifecycle()
    private val bookListViewModel: BookListViewModel by viewModel()
    private val bookListAdapter = BookListAdapter { bookListViewModel.onBookTapped(it) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        FragmentBookListBinding.inflate(inflater, container, false)
            .also {
                binding = it
                initRecyclerview()
                initObserver()
                bookListViewModel.onCreate()
            }
            .root

    private fun initRecyclerview() {
        binding.apply {
            bookList.adapter = bookListAdapter
            bookList.addOnScrollListener(
                object : PaginationScrollListener(bookList.layoutManager as LinearLayoutManager) {
                    override fun loadMoreItems() {
                        bookListViewModel.loadMoreBooks()
                    }
                    override val isLastPage: Boolean
                        get() = bookListViewModel.isLastPage
                    override val isLoading: Boolean
                        get() = bookListViewModel.isLoading
                }
            )
        }
    }

    private fun initObserver() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                bookListViewModel.viewState.collectLatest {
                    bookListAdapter.submitList(it.bookUiModels)
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                bookListViewModel.navState.collectLatest {
                    startActivity(
                        internalIntent(requireContext(), INTENT_ACTION_BOOK_DETAIL)
                            .putExtra(EXTRA_BOOK_ID, it.bookId)
                    )
                }
            }
        }
    }
}
