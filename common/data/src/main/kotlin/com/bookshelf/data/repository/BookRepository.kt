package com.bookshelf.data.repository

import arrow.core.Either
import com.bookshelf.data.datasource.remote.BookApiDataSource
import com.bookshelf.data.exception.BookDetailNotFoundException
import com.bookshelf.data.exception.EmptyBookReponseException
import com.bookshelf.data.model.BookDetailsModel
import com.bookshelf.data.model.BookModel
import com.bookshelf.data.toBookDetailModel
import com.bookshelf.data.toBookList
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

class BookRepository(private val remoteDataSource: BookApiDataSource) {

    private val bookCache: MutableSharedFlow<List<BookModel>> = MutableSharedFlow(replay = 1)

    private val isLoading: MutableSharedFlow<Boolean> = MutableSharedFlow(replay = 1)

    var hasNext: Boolean = true

    suspend fun fetchBooks(): Either<Throwable, Unit> =
        try {
            remoteDataSource
                .apply { isLoading.emit(true) }
                .getBooks()
                .run {
                    isLoading.emit(false)
                    this@BookRepository.hasNext = this.hasNext
                    if (page == 1 && books.isNotEmpty()) {
                        bookCache.emit(books.toBookList())
                        Either.Right(Unit)
                    } else {
                        Either.Left(EmptyBookReponseException)
                    }
                }
        } catch (e: Exception) {
            Either.Left(e)
        }

    fun observeBooks(): Flow<List<BookModel>> = bookCache

    fun observeLoading(): Flow<Boolean> = isLoading

    fun isLoading() = isLoading.replayCache.firstOrNull() ?: false

    suspend fun loadMoreBooks(): Either<Throwable, Boolean> {
        if (!hasNext) {
            return Either.Right(false)
        } else {
            try {
                remoteDataSource
                    .apply { isLoading.emit(true) }
                    .getBooks()
                    .run {
                        isLoading.emit(false)
                        this@BookRepository.hasNext = this.hasNext
                        val previousList: List<BookModel> =
                            bookCache.replayCache.firstOrNull() ?: emptyList()
                        val updatedList: List<BookModel> = previousList.plus(books.toBookList())
                        bookCache.emit(updatedList)
                        return Either.Right(true)
                    }
            } catch (e: Exception) {
                return Either.Left(e)
            }
        }
    }

    suspend fun getBookDetail(bookId: String): Either<Throwable, BookDetailsModel> {
        return try {
            when (val bookDetail = remoteDataSource.getBookDetail(bookId)) {
                null -> Either.Left(BookDetailNotFoundException)
                else -> Either.Right(bookDetail.toBookDetailModel())
            }
        } catch (e: Exception) {
            Either.Left(BookDetailNotFoundException)
        }
    }

    suspend fun addBook(
        title: String,
        author: String,
        price: Double,
        imageFileName: String
    ): Either<Throwable, Unit> =
        try {
            remoteDataSource.addBook(title, author, price, imageFileName)
            Either.Right(Unit)
        } catch (e: Exception) {
            Either.Left(e)
        }
}
