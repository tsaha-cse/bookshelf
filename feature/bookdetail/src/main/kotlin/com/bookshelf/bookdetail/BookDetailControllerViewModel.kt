package com.bookshelf.bookdetail

import com.bookshelf.data.model.BookDetailsModel
import com.bookshelf.data.repository.BookRepository
import com.bookshelf.platform.BaseViewModel
import com.bookshelf.platform.extension.onFailure
import com.bookshelf.platform.extension.onSuccess
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class BookDetailControllerViewModel(
    private val bookId: String,
    private val bookRepository: BookRepository
) : BaseViewModel() {

    sealed class ViewState {
        data class Data(val detailModel: BookDetailsModel) : ViewState()
        object Empty : ViewState()
        object Loading : ViewState()
    }

    private val _viewState: MutableStateFlow<ViewState> = MutableStateFlow(ViewState.Loading)
    val viewState: StateFlow<ViewState> = _viewState.asStateFlow()

    fun onCreate() {
        coroutineWrapper {
            bookRepository.getBookDetail(bookId)
                .onSuccess { coroutineWrapper { _viewState.emit(ViewState.Data(it)) } }
                .onFailure { coroutineWrapper { _viewState.emit(ViewState.Empty) } }
        }
    }
}
